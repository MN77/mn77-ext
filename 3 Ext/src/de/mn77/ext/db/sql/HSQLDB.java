/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.sql;

import de.mn77.ext.db.constant.DBMS;
import de.mn77.ext.db.login.I_JDBC_Login;


/**
 * @author Michael Nitsche
 */
public class HSQLDB extends A_SqlDB {

	public DBMS getDBMS() {
		return DBMS.HSQLDB;
	}

//	protected void createDB(final String host, final String db, final String user, final String pass) {
//		Err.todo();
//	}

	@Override
	protected boolean canTitles() {
		return true;
	}

	@Override
	protected String jdbc( final I_JDBC_Login login ) {
		// Shutdown=true greift nur, wenn die connection richtig geschlossen wird
//		a connection property, shutdown=true, can be specified on the first connection to the database (the connection that opens the database) to force a shutdown when the last connection closes.
//		String url="jdbc:hsqldb:file:/tmp/hsql/db3";//	: File-persistence
//		String url="jdbc:hsqldb:hsql:DB_NAME";//		: database server
//		String url="jdbc:hsqldb:hsql://localhost/xdb"
//		String url="jdbc:hsqldb:mem:DBname";//			: in-memory only
//		hsqls = mit TLS
//		http, https = Webserver
//		res = Zugriff auf Jar
//		http://hsqldb.org/doc/src/org/hsqldb/jdbc/jdbcConnection.html
//		http://hsqldb.org/doc/guide/ch04.html

		// InMemory
		if( !login.isSetDatabase() )
			return "jdbc:hsqldb:mem:memdb";

		// Server
		if( login.isSetHost() )
			return "jdbc:hsqldb:hsql://" + login.getHost() + "/" + login.getDatabase() + ";shutdown=true";

		// File
		return "jdbc:hsqldb:file:" + login.getDatabase() + ";shutdown=true";
	}

//	@Override
//	protected String driver() {
//		return "org.hsqldb.jdbcDriver";
//	}

}
