/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.sql;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.ArrayTitleTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_DataBase;
import de.mn77.base.sys.MOut;
import de.mn77.ext.db.login.I_JDBC_Login;


/**
 * @author Michael Nitsche
 */
public abstract class A_SqlDB implements I_SqlDB {

	private Statement  sqlStatement;
	private Connection sqlConnection;
	private boolean    connected = false;


	public void autoCommit( final boolean b ) {
		this.iCheckConnection();

		try {
			this.sqlConnection.setAutoCommit( b );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public void close() {
		if( this.connected )
			try {
				this.sqlStatement.close();
				this.sqlConnection.close();
				this.connected = false;
			}
			catch( final SQLException e ) {
				throw Err.wrap( e );
			}
	}

	public void commit() {
		this.iCheckConnection();

		try {
			this.sqlConnection.commit();
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public void connect( final I_JDBC_Login login ) throws Exception {
		this.connect( login, false );
	}

	public void connect( final I_JDBC_Login login, final boolean createDatabase ) throws Exception {
		if( this.connected )
			throw new RuntimeException( "Database already connected!" );

		if( createDatabase )
			this.createDB( login );

		try {
//			Class.forName(this.driver()); // Load driver --> Since Java 1.6. obsolete and not necessary
			this.sqlConnection = DriverManager.getConnection( this.jdbc( login ), login.getUser(), login.getPass() );
			this.sqlStatement = this.sqlConnection.createStatement();
		}
		catch( final Exception e ) {
			throw new Exception( e.getMessage() + " '" + login.toString() + "'" );
		}

		// if no exception thrown:
		this.connected = true;
	}

	public I_Table<Object> convertRSetToTableObject( final ResultSet rset, final boolean withTitles ) throws SQLException {
		I_Table<Object> result = null;

		final int width = rset.getMetaData().getColumnCount();
		result = withTitles
			? new ArrayTitleTable<>( width )
			: new ArrayTable<>( width );

		while( rset.next() ) {
			final Object[] row = new Object[width];

			for( int i = 0; i < width; i++ )
				row[i] = rset.getObject( i + 1 );

			result.add( row );
		}

		this.iSetTableTitles( rset, result, withTitles, width );

		// Clear ResultSet
//		this.resultset.beforeFirst();	// Move row-cursor back to top! (Feature not supported for HSqlDB)
		rset.close(); // TODO HACK Okay?!?
		return result;
	}

	public I_Table<String> convertRSetToTableString( final ResultSet rset, final boolean withTitles ) throws SQLException {
		I_Table<String> result = null;

		final int width = rset.getMetaData().getColumnCount();
		result = withTitles
			? new ArrayTitleTable<>( width )
			: new ArrayTable<>( width );

		while( rset.next() ) {
			final String[] row = (String[])Array.newInstance( String.class, width );

			for( int i = 0; i < width; i++ ) {
				final Object o = rset.getObject( i + 1 );
				row[i] = this.iObjectToString( o );
			}

			result.add( row );
		}

		this.iSetTableTitles( rset, result, withTitles, width );

		// Clear ResultSet
//		this.resultset.beforeFirst();	// Move row-cursor back to top! (Feature not supported for HSqlDB)
		rset.close(); // TODO HACK Okay?!?
		return result;
	}

	/**
	 * @return Returns true if the first result is a ResultSet; false if it is an update count or there are no results
	 * @apiNote Use getUpdateCount() and getResultSet() to read the Result.
	 */
	public boolean exec( final String sql ) throws Err_DataBase {
		this.iCheckConnection();
		MOut.dev( sql );

		try {
			return this.sqlStatement.execute( sql );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public Connection getConnection() {
		return this.sqlConnection;
	}

	public ResultSet getResultSet() {

		try {
			return this.sqlStatement.getResultSet();
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public int getUpdateCount() {

		try {
			return this.sqlStatement.getUpdateCount();
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	/**
	 * @return Returns the ResultSet, null is allowed
	 */
	public ResultSet query( final String sql ) {
		return this.iGetResultSet( sql, true );
	}

	// --- Single Object ---

	public boolean query_boolean( final String sql ) throws Err_DataBase {

		try {
			return this.iGetResultSet( sql, false ).getBoolean( 1 );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public double query_double( final String sql ) throws Err_DataBase {

		try {
			return this.iGetResultSet( sql, false ).getDouble( 1 );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public float query_float( final String sql ) throws Err_DataBase {

		try {
			return this.iGetResultSet( sql, false ).getFloat( 1 );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public int query_int( final String sql ) throws Err_DataBase {

		try {
			return this.iGetResultSet( sql, false ).getInt( 1 );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public long query_long( final String sql ) throws Err_DataBase {

		try {
			return this.iGetResultSet( sql, false ).getLong( 1 );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public int queryAmount( final String sql ) {
		this.iCheckConnection();
		MOut.dev( sql );

		try {
			final ResultSet rset = this.sqlStatement.executeQuery( sql );
			return rset.getFetchSize();
		}
		catch( final SQLException e ) {
			throw Err.wrap( e, sql );
		}
	}

	public I_List<String> queryColumn( final String sql ) {
		this.iCheckConnection();
		MOut.dev( sql );
		final SimpleList<String> result = new SimpleList<>();

		try {
			final ResultSet rset = this.sqlStatement.executeQuery( sql );
			final int breite = rset.getMetaData().getColumnCount();
			if( breite == 0 )
				return result;

			while( rset.next() ) {
				final Object o = rset.getObject( 1 );
				result.add( this.iObjectToString( o ) );
			}
		}
		catch( final SQLException e ) {
			throw Err.wrap( e, sql );
		}

		return result;
	}

	public I_List<Integer> queryColumnInteger( final String sql ) {
		this.iCheckConnection();
		MOut.dev( sql );
		final SimpleList<Integer> result = new SimpleList<>();

		try {
			final ResultSet rset = this.sqlStatement.executeQuery( sql );
			final int breite = rset.getMetaData().getColumnCount();
			if( breite == 0 )
				return result;

			while( rset.next() ) {
				final Object o = rset.getObject( 1 );
				result.add( this.iObjectToInteger( o ) );
			}
		}
		catch( final SQLException e ) {
			throw Err.wrap( e, sql );
		}

		return result;
	}

	public boolean queryEmpty( final String sql ) throws Err_DataBase {
//		return this.queryTable(sql).size() == 0;
		return this.queryObject( sql ) == null;
	}

	public Integer queryInteger( final String sql ) throws Err_DataBase {

		try {
			final ResultSet rs = this.iGetResultSet( sql, true );
			return rs == null || rs.getObject( 1 ) == null // getObject muss extra geprüft werden!
				? null
				: rs.getInt( 1 );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	public Object queryObject( final String sql ) {

		try {
			final ResultSet rs = this.iGetResultSet( sql, true );
			return rs == null
				? null
				: rs.getObject( 1 );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e, sql );
		}
	}

	public I_List<String> queryRow( final String sql ) {
		this.iCheckConnection();
		MOut.dev( sql );
		final SimpleList<String> result = new SimpleList<>();

		try {
			final ResultSet rset = this.sqlStatement.executeQuery( sql );
			if( rset.next() )
				for( int pos = 1; pos <= rset.getMetaData().getColumnCount(); pos++ ) {
					final Object o = rset.getObject( pos );
					result.add( this.iObjectToString( o ) );
				}
		}
		catch( final SQLException e ) {
			throw Err.wrap( e, sql );
		}

		return result;
	}

	public String queryString( final String sql ) throws Err_DataBase {

		try {
			final ResultSet rs = this.iGetResultSet( sql, true );
			return rs == null || rs.getObject( 1 ) == null // getObject muss extra geprüft werden!
				? null
				: rs.getString( 1 );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	@SuppressWarnings( "unchecked" )
	public ArrayTable<Object> queryTableObject( final String sql ) {
		return (ArrayTable<Object>)this.iQueryToTable( sql, false, false );
	}

	@SuppressWarnings( "unchecked" )
	public ArrayTable<String> queryTableString( final String sql ) {
		return (ArrayTable<String>)this.iQueryToTable( sql, false, true );
	}

	@SuppressWarnings( "unchecked" )
	public ArrayTitleTable<Object> queryTitleTableObject( final String sql ) {
		return (ArrayTitleTable<Object>)this.iQueryToTable( sql, true, false );
	}

	@SuppressWarnings( "unchecked" )
	public ArrayTitleTable<String> queryTitleTableString( final String sql ) {
		return (ArrayTitleTable<String>)this.iQueryToTable( sql, true, true );
	}

	public void rollback() {
		this.iCheckConnection();

		try {
			this.sqlConnection.rollback();
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	/**
	 * @return Returns the amount of updates
	 */
	public int update( final String sql ) throws Err_DataBase {
		this.iCheckConnection();
		MOut.dev( sql );

		try {
			final boolean hasResults = this.sqlStatement.execute( sql );
			return hasResults ? 0 : this.sqlStatement.getUpdateCount();
		}
		catch( final SQLException e ) {
			throw Err.wrap( e );
		}
	}

	protected abstract boolean canTitles();

	/**
	 * This default implementation could be overwritten!
	 */
	protected void createDB( final I_JDBC_Login login ) throws Exception {
		Err.invalid();
	}

	@Override // This is maybe bad!
	@Deprecated
	protected void finalize() throws Throwable {
		this.close();
		this.connected = false;
		super.finalize();
	}

	protected abstract String jdbc( I_JDBC_Login login );

//	protected abstract String driver();

	private void iCheckConnection() {
		if( !this.connected )
			throw new RuntimeException( "Database not connected!" );
	}

	/**
	 * @return Creates and returns a ResultSet. If the ResultSet is empty end 'nullAllowed' than 'null' will be returned, otherwise an error will be thrown.
	 */
	private ResultSet iGetResultSet( final String sql, final boolean nullAllowed ) {
		this.iCheckConnection();
		MOut.dev( sql );
		ResultSet result = null;

		try {
			result = this.sqlStatement.executeQuery( sql );
			Err.ifTooSmall( 1, result.getMetaData().getColumnCount() );
			if( !result.next() )
				if( nullAllowed )
					return null;
				else
					throw new Err_DataBase( "ResultSet contains no data", sql, result );
			if( !nullAllowed && result.getObject( 1 ) == null ) // null check
				throw new Err_DataBase( "An empty value cannot be converted", sql, result );
		}
		catch( final SQLException e ) {
			throw Err.wrap( e, sql );
		}

		return result;
	}

	// PRIVATE

	private Integer iObjectToInteger( final Object o ) {
		return o == null ? null : Integer.parseInt( o.toString() );
	}

	private String iObjectToString( final Object o ) {
		if( o == null )
			return null;

		String s = o.toString();

		if( o.getClass() == Timestamp.class )
			s = s.substring( 0, 19 );

		return s;
	}

	private I_Table<?> iQueryToTable( final String sql, final boolean withTitles, final boolean asStrings ) throws Err_DataBase {
		MOut.dev( sql );

		try {
			this.iCheckConnection();
			final ResultSet rset = this.sqlStatement.executeQuery( sql ); // never null
			return asStrings
				? this.convertRSetToTableString( rset, withTitles )
				: this.convertRSetToTableObject( rset, withTitles );
		}
		catch( final SQLException e ) {
//			throw Err.wrap(e,sql);
//			Err.show( e, "SQL-Error: " + sql );
			throw Err.wrap( e, sql );
		}
	}

	private void iSetTableTitles( final ResultSet rset, final I_Table<?> result, final boolean withTitles, final int width ) throws SQLException {

		// Set Column-Names if possible
		if( withTitles && this.canTitles() ) { // Blacklist here // Okay: MySQL, HSQLDB // Fail: SQLITE3
			final ArrayList<String> titles = new ArrayList<>( width );

			for( int columnPos = 1; columnPos <= width; columnPos++ ) {
				final String name = rset.getMetaData().getColumnName( columnPos ); //First = 1, Second = 2, ...
				titles.add( name );
			}

			((ArrayTitleTable<?>)result).setTitles( titles.toArray( new String[titles.size()] ) );
		}
	}

}
