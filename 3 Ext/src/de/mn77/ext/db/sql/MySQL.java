/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.sql;

import de.mn77.ext.db.constant.DBMS;
import de.mn77.ext.db.login.I_JDBC_Login;


/**
 * @author Michael Nitsche
 */
public class MySQL extends A_SqlDB {

	public DBMS getDBMS() {
		return DBMS.MYSQL;
	}

	@Override
	protected boolean canTitles() {
		return true;
	}

	/*
	 * "?useUnicode=true&characterEncoding=UTF-8" wird benötigt, da sonst MySQL kein €-Zeichen speichert!
	 * http://www.torsten-horn.de/techdocs/sql-utf8.htm
	 */
	@Override
	protected String jdbc( final I_JDBC_Login login ) {
		return "jdbc:mysql://" + login.getHost() + ":" + login.getPort() + "/" + login.getDatabase() + "?useUnicode=true&characterEncoding=UTF-8";
	}


//	@Override
//	protected String driver() {
//		return "com.mysql.jdbc.Driver";
//	}

//	protected void createDB(final String host, final String db, final String user, final String pass) {
//		Err.todo();
//	}

}
