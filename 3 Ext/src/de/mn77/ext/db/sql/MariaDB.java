/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.sql;

import de.mn77.ext.db.constant.DBMS;
import de.mn77.ext.db.login.I_JDBC_Login;


/**
 * @author Michael Nitsche
 */
public class MariaDB extends A_SqlDB {

	public DBMS getDBMS() {
		return DBMS.MARIADB;
	}

	@Override
	protected boolean canTitles() {
		return true;
	}

	@Override
	protected String jdbc( final I_JDBC_Login login ) {
		final StringBuilder sb = new StringBuilder();

		sb.append( "jdbc:mariadb://" );
		sb.append( login.getHost() );
		sb.append( ':' );
		sb.append( login.getPort() );
		sb.append( '/' );
		sb.append( login.getDatabase() );

		return sb.toString();

		// Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/DB?user=root&password=myPassword");
		//"?useUnicode=true&characterEncoding=UTF-8"
	}


//	protected void createDB(final String host, final String db, final String user, final String pass) {
//		Err.todo();
//	}

}
