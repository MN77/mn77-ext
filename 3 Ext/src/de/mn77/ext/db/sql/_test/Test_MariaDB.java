/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.sql._test;

import java.sql.ResultSet;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.ext.db.login.Login_MariaDB;
import de.mn77.ext.db.sql.MariaDB;


/**
 * @author Michael Nitsche
 * @created 05.09.2022
 */
public class Test_MariaDB {

	private static final String HOST = "";
	private static final String USER = "";
	private static final String PASS = "";
	private static final String DB   = "";


	public static void main( final String[] args ) {
		final Login_MariaDB login = new Login_MariaDB( Test_MariaDB.HOST, Test_MariaDB.USER, Test_MariaDB.PASS, Test_MariaDB.DB );
		final MariaDB db = new MariaDB();

		try {
			db.connect( login, false );
			final ResultSet rs = db.query( "SHOW TABLES;" );
			MOut.print( rs );
		}
		catch( final Exception e ) {
			Err.exit( e );
		}
	}

}
