/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.sql;

import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;


/**
 * @author Michael Nitsche
 * @created 26.05.2022
 */
public class Lib_SqlDriver {

	// Show available SQL-Database-Driver
	public void showDriversLong() {

		for( final Enumeration<Driver> e = DriverManager.getDrivers(); e.hasMoreElements(); ) {
			final Object o = e.nextElement();
			System.out.println( o.getClass().getName() );
			System.out.println( o.getClass() );
			System.out.println( o );
			System.out.println( o.getClass().getModule() );
			System.out.println( "---" );
		}
	}

	public void showDriversShort() {
		for( final Enumeration<Driver> e = DriverManager.getDrivers(); e.hasMoreElements(); )
			System.out.println( e.nextElement().getClass().getName() );
	}

}
