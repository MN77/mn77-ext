/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.sql;

import java.sql.Connection;
import java.sql.ResultSet;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.ArrayTitleTable;
import de.mn77.ext.db.constant.DBMS;
import de.mn77.ext.db.login.I_JDBC_Login;


/**
 * @author Michael Nitsche
 */
public interface I_SqlDB {

//	void create() throws Exception;

	void autoCommit( boolean b );

	void close();

	void commit();

	void connect( final I_JDBC_Login login ) throws Exception;

	void connect( final I_JDBC_Login login, final boolean createDatabase ) throws Exception;

	boolean exec( String sql );

	Connection getConnection();

	DBMS getDBMS();


	// --- SQL ---

	ResultSet query( String sql );

	boolean query_boolean( String sql );

	double query_double( String sql );


	// --- Query and convert ---

	float query_float( String sql );

	int query_int( String sql );

	long query_long( String sql );

	int queryAmount( String sql );

	I_List<String> queryColumn( String sql );

	I_List<Integer> queryColumnInteger( String sql );

	boolean queryEmpty( String sql );

	Integer queryInteger( String sql );


	// --- Query atomic ---

	Object queryObject( String sql );

	I_List<String> queryRow( String sql );

	String queryString( String sql );

	ArrayTable<Object> queryTableObject( final String sql );
	ArrayTable<String> queryTableString( String sql );
	ArrayTitleTable<Object> queryTitleTableObject( final String sql );
	ArrayTitleTable<String> queryTitleTableString( final String sql );

	// --- Query special ---

	int update( final String sql );


	// --- Default functions ---

//	default void createDB(final String host, final String db, final String user, final String pass) {
//		Err.invalid();
//	}

}
