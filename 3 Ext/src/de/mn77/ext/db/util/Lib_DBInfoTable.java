/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.util;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_DataBase;
import de.mn77.base.sys.MOut;
import de.mn77.ext.db.sql.A_SqlDB;
import de.mn77.ext.db.sql.MariaDB;
import de.mn77.ext.db.sql.SQLite3;


/**
 * @author Michael Nitsche
 * @created 03.12.2022
 * @implNote "key" is a reserved word in MySQL and MariaDB
 */
public class Lib_DBInfoTable {

	public static boolean checkIdent( final A_SqlDB db, final String dbIdent, final String[] dbIdentOld ) throws Err_DataBase {

		// Update old column name
		// TODO Remove this in maybe 3 month from 2022-12-04
		try {
			db.queryString( "SELECT key FROM dbinfo" );
			db.exec( "ALTER TABLE dbinfo RENAME COLUMN key TO keyid" );
			MOut.dev( "dbinfo updated" );
		}
		catch( final Throwable t ) {
//			MOut.error(t);
		}

		try {
			db.queryString( "SELECT ident FROM dbinfo" );
			db.exec( "ALTER TABLE dbinfo RENAME COLUMN ident TO keyid" );
			MOut.dev( "dbinfo updated" );
		}
		catch( final Throwable t ) {
//			MOut.error(t);
		}

		try {
			final String curIdent = db.queryString( "SELECT value FROM dbinfo WHERE keyid='name'" );

			if( curIdent.equals( dbIdent ) )
				return true;

			boolean isOld = false;
			if( dbIdentOld != null )
				for( final String s : dbIdentOld )
					if( curIdent.equals( s ) ) {
						isOld = true;
						break;
					}

			if( isOld ) {
				db.update( "UPDATE dbinfo SET value='" + dbIdent + "' WHERE keyid='name'" );
				return true;
			}

			return false;
		}
		catch( final Err_DataBase e ) {
			throw e;
		}
		catch( final Exception e ) {
			throw new Err_DataBase( e );
		}
	}

	public static void create( final A_SqlDB db, final String dbIdent ) {
		String createTable = null;

		if( db instanceof MariaDB )
			createTable = "" +
				"CREATE TABLE dbinfo(" +
				"id INTEGER NOT NULL AUTO_INCREMENT," +
				"keyid VARCHAR(16) NOT NULL," +
				"value VARCHAR(256) NOT NULL," + // 128?
				"PRIMARY KEY(id))";
		else if( db instanceof SQLite3 )
			createTable = "" +
				"CREATE TABLE dbinfo(" +
				"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"keyid VARCHAR(16) NOT NULL UNIQUE," +
				"value VARCHAR(256) NOT NULL" + // 128?
				")";
		else
			Err.invalid( db.getClass().getSimpleName() );

//		MOut.print(sql);
		db.exec( createTable );
		db.exec( "INSERT INTO dbinfo (keyid,value) VALUES ('name','" + dbIdent + "')" );
		db.exec( "INSERT INTO dbinfo (keyid,value) VALUES ('version', 1)" );
	}

	public static void setVersion( final A_SqlDB db, final int version ) {
		Err.ifTooSmall( 1, version );
		db.exec( "UPDATE dbinfo SET value=" + version + " WHERE keyid='version'" );
	}

}
