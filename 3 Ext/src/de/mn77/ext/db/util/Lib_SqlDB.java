/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.util;

import de.mn77.base.data.form.FormString;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.ext.db.sql.I_SqlDB;


/**
 * @author Michael Nitsche
 * @created 2010-12-12
 */
public class Lib_SqlDB {

	public static boolean existTable( final I_SqlDB db, final String tableName ) {
		Err.ifNull( tableName );
		final String sql = "SELECT COUNT(*) FROM " + FormString.toSQL_Identifier( tableName );

		try {
			db.query_int( sql ); //Wirft einen Fehler, wenn die Tab noch nicht existiert!
			return true;
		}
		catch( final Err_Runtime e ) {
			return false;
		}
	}

}
