/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.util;

import de.mn77.base.error.Err;
import de.mn77.ext.db.constant.DBMS;


/**
 * @author Michael Nitsche
 * @created 2009-01-29
 */
public class Lib_Sql {

	public static String fieldLastID( final DBMS dbms ) {

		switch( dbms ) {
			case HSQLDB:
				return "IDENTITY()";
			case MYSQL:
			case MARIADB:
				return "LAST_INSERT_ID()";
			case SQLITE3:
				return "LAST_INSERT_ROWID()";
			default:
				throw Err.todo();
		}
	}

}
