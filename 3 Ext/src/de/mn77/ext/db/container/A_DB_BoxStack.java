/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.container;

import java.util.Map;

import de.mn77.base.data.container.A_BoxStack;
import de.mn77.base.data.container.I_Box;


/**
 * @author Michael Nitsche
 * @created 10.03.2009
 * @implNote
 *           ACHTUNG! Beim Umbenennen oder Verschieben unbedingt die proguard-Configs anpassen!!!
 */
public abstract class A_DB_BoxStack extends A_BoxStack {

	/** TODO Jede Klasse muß/sollte eine INFO-Variable implementieren. Irgendwie abstract lösen! **/
//	public static final B_FooBar info = new B_FooBar();


	public String getError() {
		final Map<String, I_Box<?>> boxes = this.getBoxes();

		for( final String key : boxes.keySet() ) {
			final String box = ((DBBox<?>)boxes.get( key )).getError();
			if( box != null )
				return key + ": " + box;
		}

		return null;
	}

	public boolean isValid() {
		final Map<String, I_Box<?>> boxes = this.getBoxes();
		for( final String key : boxes.keySet() )
			if( !((DBBox<?>)boxes.get( key )).isValid() )
				return false;
		return true;
	}

}
