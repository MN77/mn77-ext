/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.container;

import java.util.Map.Entry;

import de.mn77.base.data.container.I_Box;
import de.mn77.base.data.datetime.I_Date;
import de.mn77.base.data.datetime.I_DateTime;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 20.08.2009
 */
public final class DBBox<T> implements I_Box<T> {

	public final Class<T>       type;
	public final Integer        size;
	public final boolean        primaryKey;
	public final boolean        notNull;
	public final boolean        autoCounter;
	public final T              defaultValue;
	private final A_DB_BoxStack stack;
	private final boolean       update;
	private String              name  = null;
	private boolean             empty = true;
	private T                   value = null;


	public static DBBox<Boolean> newBool( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, Boolean.class, null, null, notnull, false, false, false );
	}


//	public static <T1> DBBox<T1> neu(final A_DB_BoxStack box, final Class<T1> typ, final Integer size, final boolean notnull) {
//		return new DBBox<>(this.box, this.typ, (T1)null, this.size, this.notnull, false, false, false);
//	}
//
//	public static <T> DBBox<T> neu(final A_DB_BoxStack box, final Class<T> typ, final Integer size, final boolean notnull, final T init) {
//		final DBBox<T> result = new DBBox<>(this.box, this.typ, this.init, this.size, this.notnull, false, false, false);
//		result.set(this.init);
//		return result;
//	}

	public static DBBox<Boolean> newBool( final A_DB_BoxStack box, final boolean notnull, final boolean standard ) {
		return new DBBox<>( box, Boolean.class, standard, null, notnull, false, false, false );
	}

	public static DBBox<I_Date> newDatum( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, I_Date.class, null, null, notnull, false, false, false );
	}

	public static DBBox<Integer> newInteger( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, Integer.class, null, null, notnull, false, false, false );
	}

	public static DBBox<Integer> newInteger( final A_DB_BoxStack box, final boolean notnull, final int standard ) {
		return new DBBox<>( box, Integer.class, standard, null, notnull, false, false, false );
	}

	public static DBBox<Long> newLong( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, Long.class, null, null, notnull, false, false, false );
	}

	public static DBBox<Long> newLong( final A_DB_BoxStack box, final boolean notnull, final long standard ) {
		return new DBBox<>( box, Long.class, standard, null, notnull, false, false, false );
	}

	public static DBBox<Integer> newPrimaryKey( final A_DB_BoxStack box, final boolean autoincrement ) {
		return new DBBox<>( box, Integer.class, null, null, true, true, autoincrement, false );
	}

	public static DBBox<Long> newPrimaryKeyLong( final A_DB_BoxStack box, final boolean autoincrement ) {
		return new DBBox<>( box, Long.class, null, null, true, true, autoincrement, false );
	}

	public static DBBox<String> newString( final A_DB_BoxStack box, final int size, final boolean notnull ) {
		return new DBBox<>( box, String.class, null, size, notnull, false, false, false );
	}

	public static DBBox<String> newString( final A_DB_BoxStack box, final int size, final boolean notnull, final String standard ) {
		return new DBBox<>( box, String.class, standard, size, notnull, false, false, false );
	}

	public static DBBox<String> newStringFlex( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, String.class, null, null, notnull, false, false, false );
	}

	public static DBBox<I_DateTime> newZeitpunkt( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, I_DateTime.class, null, null, notnull, false, false, false );
	}

	public static <T> DBBox<T> update( final A_DB_BoxStack box, final Class<T> typ, final Integer size, final boolean notnull, final T init ) {
		final DBBox<T> result = new DBBox<>( box, typ, init, size, notnull, false, false, true );
		result.set( init );
		return result;
	}

	// UPDATES

	public static <T1> DBBox<T1> update( final A_DB_BoxStack box, final Class<T1> typ, final Integer size, final boolean notnull ) {
		return new DBBox<>( box, typ, (T1)null, size, notnull, false, false, true );
	}

	public static DBBox<Boolean> updateBool( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, Boolean.class, null, null, notnull, false, false, true );
	}

	public static DBBox<Boolean> updateBool( final A_DB_BoxStack box, final boolean notnull, final boolean standard ) {
		return new DBBox<>( box, Boolean.class, standard, null, notnull, false, false, true );
	}

	public static DBBox<I_Date> updateDatum( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, I_Date.class, null, null, notnull, false, false, true );
	}

	public static DBBox<Integer> updateInteger( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, Integer.class, null, null, notnull, false, false, true );
	}

	public static DBBox<Integer> updateInteger( final A_DB_BoxStack box, final boolean notnull, final Integer standard ) {
		return new DBBox<>( box, Integer.class, standard, null, notnull, false, false, true );
	}

	public static DBBox<Integer> updatePrimaryKey( final A_DB_BoxStack box, final boolean autoincrement ) {
		return new DBBox<>( box, Integer.class, null, null, true, true, autoincrement, true );
	}

	public static DBBox<String> updateString( final A_DB_BoxStack box, final int size, final boolean notnull ) {
		return new DBBox<>( box, String.class, null, size, notnull, false, false, true );
	}

	public static DBBox<String> updateStringFlex( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, String.class, null, null, notnull, false, false, true );
	}

	public static DBBox<I_DateTime> updateZeitpunkt( final A_DB_BoxStack box, final boolean notnull ) {
		return new DBBox<>( box, I_DateTime.class, null, null, notnull, false, false, true );
	}

	private DBBox( final A_DB_BoxStack stack, final Class<T> typ, final T defaultValue, final Integer size, final boolean notNull, final boolean primaryKey, final boolean autoCounter,
		final boolean update ) {
		this.stack = stack;
		this.type = typ;
		this.size = size;
		this.notNull = notNull;
		this.primaryKey = primaryKey;
		this.autoCounter = autoCounter;
		this.defaultValue = defaultValue;
		this.update = update;
	}


	// FUNCTIONS

	public DBBox<T> copy() {
		final DBBox<T> copy = new DBBox<>( this.stack, this.type, this.defaultValue, this.size, this.notNull, this.primaryKey, this.autoCounter, this.update );
		copy.name = this.name;
		copy.empty = this.empty;
		copy.value = this.value;
		return copy;
	}

	public T get() {
		return this.value;
	}

	public Group2<Boolean, T> getCopy() {
		return new Group2<>( this.empty, this.value );
	}

	public T getDefaultValue() {
		return this.defaultValue;
	}

	public String getError() {
		if( !this.primaryKey && this.notNull && this.value == null ) //Bei neuer Eingabe ist ID z.B. immer Null!
			return "Pflichtfeld ist NULL";
		if( this.type == String.class && ("" + this.value).length() > this.size )
			return "Text ist zu lang";

		return null;
	}

	public String getName() {
		if( this.name != null )
			return this.name;

		for( final Entry<String, I_Box<?>> e : this.stack.getBoxes().entrySet() )
			if( e.getValue().equals( this ) )
				return e.getKey().toLowerCase();

		throw Err.invalid( this );
	}

	public Integer getSize() {
		return this.size;
	}

	public Class<T> getType() {
		return this.type;
	}

	public boolean isAutoCounter() {
		return this.autoCounter;
	}

	public boolean isEmpty() {
		return this.empty;
	}

	public boolean isNotNull() {
		return this.notNull;
	}

//	public A_DB_BoxStack getBoxStack()	{ return this.box; }

	public boolean isPrimaryKey() {
		return this.primaryKey;
	}

	public boolean isRequired() {
		return this.notNull; // TODO check it!
	}

	public boolean isUpdate() {
		return this.update;
	}

	public boolean isValid() {
		return this.getError() == null;
	}

	public void set( final T value ) {
		if( this.notNull && value == null )
			Err.forbidden( "A required field can not be NULL!", this.getName() );
		this.value = value;
		this.empty = false;
	}

	public void setCopy( final Group2<Boolean, T> g ) {
		this.empty = g.o1;
		this.value = g.o2;
	}

	@Override
	public String toString() { //Evtl. ohne Box()
		return this.getClass().getSimpleName() + "(" + this.get() + ")";
	}

}
