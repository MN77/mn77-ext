/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.login;

/**
 * @author Michael Nitsche
 * @created 30.03.2010
 */
public class JDBC_Login implements I_JDBC_Login {

	public final String  host;
	public final Integer port;
	public final String  user;
	public final String  db;
	public final String  pass;


	public JDBC_Login( final String host, final Integer port, final String user, final String pass, final String db ) {
		this.host = host;
		this.port = port;
		this.db = db;
		this.user = user;
		this.pass = pass;
	}

	public String getDatabase() {
		return this.db;
	}

	public String getHost() {
		return this.host;
	}

	public String getPass() {
		return this.pass;
	}

	public Integer getPort() {
		return this.port;
	}

	public String getUser() {
		return this.user;
	}

	public boolean isSetDatabase() {
		return this.db != null && this.db.length() > 0;
	}

	public boolean isSetHost() {
		return this.host != null && this.host.length() > 0;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		if( this.user != null && this.user.length() > 0 ) {
			sb.append( this.user );
			sb.append( '@' );
		}

		if( this.host != null && this.host.length() > 0 )
			sb.append( this.host );

		if( this.port != null ) {
			sb.append( ':' );
			sb.append( this.port );
		}

		if( this.db != null && this.db.length() > 0 ) {
			sb.append( '/' );
			sb.append( this.db );
		}

		return sb.toString();
	}

}
