/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.login;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 05.09.2022
 */
public class Login_MariaDB extends JDBC_Login {

	public static Login_MariaDB from( final String[] login ) {
		Err.ifNull( login );

		switch( login.length ) {
			case 4:
				return new Login_MariaDB( login[0], login[1], login[2], login[3] );
			case 5:
				return new Login_MariaDB( login[0], Integer.parseInt( login[1] ), login[2], login[3], login[4] );
			default:
				throw Err.direct( "Invalid login data! Need 4 or 5 arguments. Got: " + login.length );
		}
	}

	public Login_MariaDB( final String host, final int port, final String user, final String pass, final String db ) {
		super( host, port, user, pass, db );
	}

	public Login_MariaDB( final String host, final String user, final String pass, final String db ) {
		super( host, 3306, user, pass, db );
	}

}
