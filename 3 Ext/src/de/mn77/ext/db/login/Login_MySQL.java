/*******************************************************************************
 * Copyright (C) 2008-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.db.login;

/**
 * @author Michael Nitsche
 * @created 10.08.2008
 */
public class Login_MySQL extends JDBC_Login {

	public Login_MySQL( final String host, final Integer port, final String user, final String pass, final String db ) {
		super( host, port, user, pass, db );
	}

	public Login_MySQL( final String host, final String user, final String pass, final String db ) {
		super( host, 3306, user, pass, db );
	}

}
