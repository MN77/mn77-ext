/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.marytts._test;

import de.mn77.base.sys.MOut;
import de.mn77.ext.marytts.MaryTTS;


/**
 * @author Michael Nitsche
 * @created 15.06.2018
 */
public class MaryTTS_Test {

	public static void main( final String[] args ) {
		final MaryTTS process = new MaryTTS();
		process.startService();

//		MOut.temp(Sys.getCurrentDir());

		MOut.temp( process.getVoices() );

		// Benötigt wird immer die Sprachumgebungs-Jar (de,en,it,...) und dann die jeweilige Sprache
		// Bei den großen Sprach-Dateien muss am besten das lib-Verzeichnis verlinkt werden, oder die mary-Variable beim start gesetzt werden.

		// Wichtig!!!
		// VM-Argument: -Dmary.base=/home/mike/Prog/Java/Lib/marytts

		// --------------------------------------------------------

		// de
//		process.setVoiceName("bits1-hsmm");
//		process.setVoiceName("bits3-hsmm");

//		process.setVoiceName("dfki-pavoque-neutral");
		process.setVoiceName( "dfki-pavoque-neutral-hsmm" );

		// en
//		process.setVoiceName("cmu-rms-hsmm");	// Cool
//		process.setVoiceName("cmu-bdl-hsmm");
//		process.setVoiceName("cmu-slt-hsmm");

//		process.setVoiceName("cmu-rms");
//		process.setVoiceName("cmu-bdl");
//		process.setVoiceName("cmu-slt");

		// it
//		process.setVoiceName("istc-lucia-hsmm");

		// --------------------------------------------------------

//		final String s = "Elon Musk bringt einen neuen Tesla auf den Markt.";

		// de
		final String s = "Hallo!";
//		final String s="Hallo Michael. Hallo Maria! Wie geht's euch?";
//		final String s = "Es gibt ein einfaches Grundprinzip, mit dem wir, wenn wir uns wirklich daran halten, alle Probleme lösen können und uns harmonisch miteinander verbinden können.";

		// en
//		final String s="There is a simple rationale that, if we really stick to it, can solve all problems and connect us harmoniously.";
//		final String s="Thanks to the flexibility to be used in current and in voltage mode it is also a perfect device for powering non LED applications like USB and mobile wireless charger or as multipurpose pre-regulation like for infotainment.";

		// it
//		final String s="C'è una logica semplice che, se ci atteniamo davvero a tutto, possiamo risolvere tutti i problemi e connetterci armoniosamente.";
//		final String s = "Non penso sia falso - scrive una donna su Facebook - Ho visto da vicino fenomini simili per decenni a Big Island. La lava si può muovere così velocemente se il canale è profondo e c'è una pendenza come in questo caso, con il magma diretto verso l'oceano a Kapoho Bay.";

		// es
//		final String s="Existe un razonamiento simple que, si realmente lo mantenemos, puede resolver todos los problemas y conectarnos armoniosamente.";

		// --------------------------------------------------------

//		process.textToSpeach("Hallo!");
//		Sys.sleepSeconds(2);
//
//		process.textToSpeach("Herzlich Willkommen!");
//		Sys.sleepSeconds(2);

		process.textToSpeach( s, true );

		MOut.print( "Finished" );
	}

}
