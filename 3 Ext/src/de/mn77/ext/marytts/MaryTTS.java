/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.marytts;

import java.io.IOException;
import java.util.Locale;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineListener;

import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import marytts.LocalMaryInterface;
import marytts.MaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import marytts.util.data.audio.AudioPlayer;
import marytts.util.data.audio.MaryAudioUtils;


public class MaryTTS {

	private MaryInterface marytts;
	private String        outputFile = null;
	private String        voiceName  = null;


	public I_Table<String> getVoices() {
		if( this.marytts == null )
			throw Err.invalid( "First start the Service!" );

		final ArrayTable<String> tab = new ArrayTable<>( 2 );

		final Iterable<Locale> locales = this.marytts.getAvailableLocales();

		for( final Locale locale : locales ) {
			final Iterable<String> voices = this.marytts.getAvailableVoices( locale );
			for( final String voice : voices )
				tab.addRow( locale.toString(), voice );
		}

		return tab;
	}

	public void setOutputFile( final String outputFile ) {
		this.outputFile = outputFile;
	}

	public void setVoiceName( final String voiceName ) {
		this.voiceName = voiceName;
	}

	public void startService() {
		if( this.marytts != null )
			throw Err.invalid( "MaryTTS is allready started!" );

		try {
			this.marytts = new LocalMaryInterface();
		}
		catch( final MaryConfigurationException e ) {
			throw new Err_Runtime( e );
		}
	}

	public void stopService() {
		this.marytts = null; //TODO is it really stopped?!?
	}

	public void textToSpeach( final String text, final boolean wait ) {
		if( this.marytts == null )
			throw Err.invalid( "MaryTTS-Process not started!" );

		if( text.trim().length() == 0 )
			return;

		if( this.voiceName != null && !this.voiceName.isEmpty() ) {
			this.marytts.setVoice( this.voiceName );
			this.voiceName = null;
		}

		AudioInputStream audio = null; //TODO setup output
		final String toSpeak = FilterString.removeEmptyLines( text );

		// Process
		try {
			audio = this.marytts.generateAudio( toSpeak );
		}
		catch( final SynthesisException e ) {
			throw new Err_Runtime( e );
		}

		// Output audio as file/sound
		if( this.outputFile != null && !this.outputFile.isEmpty() )
			try {
				MaryAudioUtils.writeWavFile( MaryAudioUtils.getSamplesAsDoubleArray( audio ), this.outputFile, audio.getFormat() );
			}
			catch( final IOException e ) {
				throw new Err_Runtime( e );
			}
		else
			this.iPlayAudio( audio, wait );
	}

	private void iPlayAudio( final AudioInputStream audio, final boolean wait ) {
		final LineListener lineListener = event -> {
			// Event types: Open Start Stop Close
//			if(event.getType() == LineEvent.Type.START) {
//			MOut.print("--- Line listener ---", "Line: "+event.getLine(), "FramePosition: "+event.getFramePosition(), "Source: "+event.getSource(), "Type: "+event.getType(), "Event: "+event);
		};

		final AudioPlayer ap = new AudioPlayer( audio, lineListener );

		try {
			ap.start();
			if( wait )
				ap.join();
		}
		catch( final InterruptedException e ) {
			Err.show( e );
		}
	}

}
