/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create.page;

import java.util.ArrayList;

import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;

import de.mn77.base.data.constant.position.POSITION_H;
import de.mn77.base.error.Err;
import de.mn77.ext.pdf.create.constant.PDF_FONT;
import de.mn77.ext.pdf.create.constant.PDF_PAGEFORMAT;
import de.mn77.ext.pdf.create.page.item.I_PDF_Item;
import de.mn77.ext.pdf.create.page.item.PDF_ItemCircle;
import de.mn77.ext.pdf.create.page.item.PDF_ItemImage;
import de.mn77.ext.pdf.create.page.item.PDF_ItemLine;
import de.mn77.ext.pdf.create.page.item.PDF_ItemRectangle;
import de.mn77.ext.pdf.create.page.item.PDF_ItemText;
import de.mn77.lib.graphic.I_Image;


/**
 * @author Michael Nitsche
 */
public class PDF_Page {

	private boolean                     isLandscape;
	private PDF_PAGEFORMAT              format;
	private final ArrayList<I_PDF_Item> items;
	private float                       lineThickness;
	private int[]                       colorBackground, colorLine, colorFont, colorFill;
	private int                         fontsize;
	private PDF_FONT                    font;
	private POSITION_H                  fontAlignment;


	public PDF_Page() {
		this.items = new ArrayList<>();
		this.isLandscape = false;
		this.format = PDF_PAGEFORMAT.A4;
		this.lineThickness = 1;
		this.colorBackground = new int[]{ 255, 255, 255 };
		this.colorLine = new int[]{ 0, 0, 0 };
		this.colorFont = new int[]{ 0, 0, 0 };
		this.colorFill = new int[]{ 0, 0, 0 };
		this.fontsize = 12;
		this.font = PDF_FONT.HELVETICA;
		this.fontAlignment = POSITION_H.LEFT;
	}

	public void draw( final PdfContentByte cb ) {
		Rectangle rect = this.format.getRectangle();
		if( this.isLandscape )
			rect = rect.rotate();
//		format.setBackgroundColor(new Color(this.hintergrund[0],this.hintergrund[1],this.hintergrund[2])); //FIXME: RectangleReadOnly: this Rectangle is read only. //Siehe auch PDF_Dokument
		for( final I_PDF_Item e : this.items )
			e.draw( this, cb );
	}

	public int[] getBackground() {
		return this.colorBackground;
	}

	public PDF_PAGEFORMAT getFormat() {
		return this.format;
	}

	public float getHeight() {
		Rectangle format = this.format.getRectangle();
		if( this.isLandscape )
			format = format.rotate();
		return format.getHeight();
	}

	public float getWidth() {
		Rectangle rect = this.format.getRectangle();
		if( this.isLandscape )
			rect = rect.rotate();
		return rect.getWidth();
	}

	public boolean isLandscape() {
		return this.isLandscape;
	}

	public PDF_ItemCircle newCircle( final float x, final float y, final float r ) {
		final PDF_ItemCircle result = new PDF_ItemCircle( x, y, r );
		result.setLineThickness( this.lineThickness );
		result.setBorderColor( this.colorLine[0], this.colorLine[1], this.colorLine[2] );
		result.setFillColor( this.colorFill[0], this.colorFill[1], this.colorFill[2] );
		this.items.add( result );
		return result;
	}

	public PDF_ItemImage newImage( final I_Image image, final float x, final float y ) {
		final PDF_ItemImage result = new PDF_ItemImage( image, x, y, image.getWidth(), image.getHeight() );
		this.items.add( result );
		return result;
	}

	public PDF_ItemImage newImage( final I_Image image, final float x, final float y, final int dx, final int dy ) {
		final PDF_ItemImage result = new PDF_ItemImage( image, x, y, dx, dy );
		this.items.add( result );
		return result;
	}

	public PDF_ItemLine newLine( final float x1, final float y1, final float x2, final float y2 ) {
		final PDF_ItemLine result = new PDF_ItemLine( x1, y1, x2, y2 );
		result.setThickness( this.lineThickness );
		result.setColor( this.colorLine[0], this.colorLine[1], this.colorLine[2] );
		this.items.add( result );
		return result;
	}

	public PDF_ItemRectangle newRectangle( final float x, final float y, final float dx, final float dy ) {
		final PDF_ItemRectangle result = new PDF_ItemRectangle( x, y, dx, dy );
		result.setThickness( this.lineThickness );
		result.setBorderColor( this.colorLine[0], this.colorLine[1], this.colorLine[2] );
		result.setFillColor( this.colorFill[0], this.colorFill[1], this.colorFill[2] );
		this.items.add( result );
		return result;
	}

	public PDF_ItemText newText( final float x, final float y, final String text ) {
		final PDF_ItemText result = new PDF_ItemText( x, y, text );
		result.setColor( this.colorFont[0], this.colorFont[1], this.colorFont[2] );
		result.setFont( this.font );
		result.setFontsize( this.fontsize );
		result.setAlignment( this.fontAlignment );
		this.items.add( result );
		return result;
	}

	public PDF_Page setBackground( final int... rgb ) {
		Err.ifNot( 3, rgb.length );
		Err.ifOutOfBounds( 0, 255, rgb[0] );
		Err.ifOutOfBounds( 0, 255, rgb[1] );
		Err.ifOutOfBounds( 0, 255, rgb[2] );
		this.colorBackground = rgb;
		return this;
	}

	public PDF_Page setFillColor( final int... rgb ) {
		Err.ifNot( 3, rgb.length );
		Err.ifOutOfBounds( 0, 255, rgb[0] );
		Err.ifOutOfBounds( 0, 255, rgb[1] );
		Err.ifOutOfBounds( 0, 255, rgb[2] );
		this.colorFill = rgb;
		return this;
	}

	public PDF_Page setFont( final PDF_FONT schrift ) {
		Err.ifNull( schrift );
		this.font = schrift;
		return this;
	}

	public PDF_Page setFontAlignment( final POSITION_H align ) {
		this.fontAlignment = align;
		return this;
	}

	public PDF_Page setFontColor( final int... rgb ) {
		Err.ifNot( 3, rgb.length );
		Err.ifOutOfBounds( 0, 255, rgb[0] );
		Err.ifOutOfBounds( 0, 255, rgb[1] );
		Err.ifOutOfBounds( 0, 255, rgb[2] );
		this.colorFont = rgb;
		return this;
	}

	public PDF_Page setFontSize( final int size ) {
		Err.ifOutOfBounds( 4, 100, size );
		this.fontsize = size;
		return this;
	}

	public PDF_Page setFormat( final PDF_PAGEFORMAT format ) {
		Err.ifNull( format );
		this.format = format;
		return this;
	}

	public PDF_Page setLineColor( final int... rgb ) {
		Err.ifNot( 3, rgb.length );
		Err.ifOutOfBounds( 0, 255, rgb[0] );
		Err.ifOutOfBounds( 0, 255, rgb[1] );
		Err.ifOutOfBounds( 0, 255, rgb[2] );
		this.colorLine = rgb;
		return this;
	}

	public PDF_Page setOrientation( final boolean isLandscape ) {
		this.isLandscape = isLandscape;
		return this;
	}

	public PDF_Page setThickness( final float thickness ) {
		Err.ifOutOfBounds( 0.1, 20, thickness );
		this.lineThickness = thickness;
		return this;
	}

}
