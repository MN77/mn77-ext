/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create.page.item;

import java.awt.Color;

import com.lowagie.text.pdf.PdfContentByte;

import de.mn77.base.error.Err;
import de.mn77.ext.pdf.create.page.PDF_Page;


/**
 * @author Michael Nitsche
 */
public class PDF_ItemLine implements I_PDF_Item {

	private final float x1, x2, y1, y2;
	private float       thickness;
	private int[]       colorRGB;


	public PDF_ItemLine( final float x1, final float y1, final float x2, final float y2 ) {
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		this.thickness = 1;
		this.colorRGB = new int[]{ 0, 0, 0 };
	}

	public void draw( final PDF_Page page, final PdfContentByte cb ) {
		cb.setColorStroke( new Color( this.colorRGB[0], this.colorRGB[1], this.colorRGB[2] ) );
		cb.setLineWidth( this.thickness );
		cb.moveTo( this.x1, page.getHeight() - this.y1 );
		cb.lineTo( this.x2, page.getHeight() - this.y2 );
		cb.stroke();
	}

	public PDF_ItemLine setColor( final int r, final int g, final int b ) {
		Err.ifOutOfBounds( 0, 255, r );
		Err.ifOutOfBounds( 0, 255, g );
		Err.ifOutOfBounds( 0, 255, b );
		this.colorRGB = new int[]{ r, g, b };
		return this;
	}

	public PDF_ItemLine setThickness( final double thickness ) {
		Err.ifOutOfBounds( 0, 50, thickness );
		this.thickness = (float)thickness;
		return this;
	}

}
