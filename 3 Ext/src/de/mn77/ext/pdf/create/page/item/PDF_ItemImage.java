/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create.page.item;

import java.awt.Point;

import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfContentByte;

import de.mn77.base.error.Err;
import de.mn77.ext.pdf.create.page.PDF_Page;
import de.mn77.lib.graphic.I_Image;
import de.mn77.lib.graphic.Lib_Graphic;


/**
 * @author Michael Nitsche
 */
public class PDF_ItemImage implements I_PDF_Item {

	private final float   x, y;
	private final int     dx, dy;
	private final I_Image image;


	public PDF_ItemImage( final I_Image image, final float x, final float y, final int dx, final int dy ) {
		this.image = image;
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
	}

	public void draw( final PDF_Page page, final PdfContentByte cb ) {

		try {
			final Image b = Image.getInstance( this.image.getImage(), null, false );
			final Point p = Lib_Graphic.calcResize( this.image.getWidth(), this.image.getHeight(), this.dx, this.dy );
			b.scaleAbsolute( p.x, p.y );
			b.setAbsolutePosition( this.x, page.getHeight() - this.y - p.y );
			cb.addImage( b );
		}
		catch( final Exception e ) {
			Err.show( e );
		}
	}

}
