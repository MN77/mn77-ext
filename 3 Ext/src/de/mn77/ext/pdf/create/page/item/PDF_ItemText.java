/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create.page.item;

import java.awt.Color;

import com.lowagie.text.pdf.PdfContentByte;

import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.constant.position.POSITION_H;
import de.mn77.base.error.Err;
import de.mn77.ext.pdf.create.constant.PDF_FONT;
import de.mn77.ext.pdf.create.page.PDF_Page;


/**
 * @author Michael Nitsche
 */
public class PDF_ItemText implements I_PDF_Item {

	private final String text;
	private final float  x, y;
	private int[]        rgb;
	private PDF_FONT     font;
	private int          fontsize;
	private boolean      bold, italic;
	private POSITION_H   alignment;


	public PDF_ItemText( final float x, final float y, final String text ) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.rgb = new int[]{ 0, 0, 0 };
		this.font = PDF_FONT.HELVETICA;
		this.fontsize = 12;
		this.bold = false;
		this.italic = false;
		this.alignment = POSITION_H.LEFT;
	}

	public void draw( final PDF_Page page, final PdfContentByte cb ) {
		cb.setColorFill( new Color( this.rgb[0], this.rgb[1], this.rgb[2] ) );
		cb.beginText();
		cb.setFontAndSize( this.font.getBaseFont( this.bold, this.italic ), this.fontsize );

		int align = PdfContentByte.ALIGN_LEFT;
		if( this.alignment == POSITION.RIGHT )
			align = PdfContentByte.ALIGN_RIGHT;
		if( this.alignment == POSITION.CENTER )
			align = PdfContentByte.ALIGN_CENTER;
		cb.showTextAligned( align, this.text, this.x, page.getHeight() - this.y, 0 );
		cb.endText();
	}

	public PDF_ItemText setAlignment( final POSITION_H a ) {
		this.alignment = a;
		return this;
	}

	public PDF_ItemText setAlignRight() {
		this.alignment = POSITION_H.RIGHT;
		return this;
	}

	public PDF_ItemText setBold() {
		this.bold = true;
		return this;
	}

	public PDF_ItemText setColor( final int... rgb ) {
		Err.ifNot( 3, rgb.length );
		Err.ifOutOfBounds( 0, 255, rgb[0] );
		Err.ifOutOfBounds( 0, 255, rgb[1] );
		Err.ifOutOfBounds( 0, 255, rgb[2] );
		this.rgb = rgb;
		return this;
	}

	public PDF_ItemText setFont( final PDF_FONT font ) {
		Err.ifNull( font );
		this.font = font;
		return this;
	}

	public PDF_ItemText setFontsize( final int fontsize ) {
		Err.ifOutOfBounds( 4, 150, fontsize );
		this.fontsize = fontsize;
		return this;
	}

	public PDF_ItemText setItalic() {
		this.italic = true;
		return this;
	}

}
