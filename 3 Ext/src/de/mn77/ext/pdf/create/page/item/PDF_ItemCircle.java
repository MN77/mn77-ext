/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create.page.item;

import java.awt.Color;

import com.lowagie.text.pdf.PdfContentByte;

import de.mn77.base.error.Err;
import de.mn77.ext.pdf.create.page.PDF_Page;


/**
 * @author Michael Nitsche
 */
public class PDF_ItemCircle implements I_PDF_Item {

	private final float x, y, r;
	private float       thickness;
	private int[]       colorFill, colorBorder;
	private boolean     isFilled, hasBorder;


	public PDF_ItemCircle( final float x, final float y, final float r ) {
		this.x = x;
		this.y = y;
		this.r = r;
		this.thickness = 1;
		this.colorBorder = new int[]{ 0, 0, 0 };
		this.colorFill = new int[]{ 0, 0, 0 };
		this.isFilled = false;
		this.hasBorder = true;
	}

	public void draw( final PDF_Page page, final PdfContentByte cb ) {

		if( this.isFilled ) {
			cb.setColorFill( new Color( this.colorFill[0], this.colorFill[1], this.colorFill[2] ) );
			cb.circle( this.x, page.getHeight() - this.y, this.r );
			cb.fill();
			cb.stroke();
		}

		if( this.hasBorder ) {
			cb.setColorStroke( new Color( this.colorBorder[0], this.colorBorder[1], this.colorBorder[2] ) );
			cb.setLineWidth( this.thickness );
			cb.circle( this.x, page.getHeight() - this.y, this.r );
			cb.stroke();
		}
	}

	public PDF_ItemCircle setBorderColor( final int r, final int g, final int b ) {
		Err.ifOutOfBounds( 0, 255, r );
		Err.ifOutOfBounds( 0, 255, g );
		Err.ifOutOfBounds( 0, 255, b );
		this.colorBorder = new int[]{ r, g, b };
		return this;
	}

	public PDF_ItemCircle setFillColor( final int r, final int g, final int b ) {
		Err.ifOutOfBounds( 0, 255, r );
		Err.ifOutOfBounds( 0, 255, g );
		Err.ifOutOfBounds( 0, 255, b );
		this.colorFill = new int[]{ r, g, b };
		return this;
	}

	public PDF_ItemCircle setLineThickness( final float staerke ) {
		Err.ifOutOfBounds( 0, 50, staerke );
		this.thickness = staerke;
		return this;
	}

	public PDF_ItemCircle setStyle( final boolean isFilled, final boolean hasBorder ) {
		if( !isFilled && !hasBorder )
			Err.invalid( "A rectangle without fillig and border could not be drawed!" );
		this.isFilled = isFilled;
		this.hasBorder = hasBorder;
		return this;
	}

}
