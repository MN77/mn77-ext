/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create._test;

import de.mn77.base.data.constant.position.POSITION_H;
import de.mn77.base.error.Err;
import de.mn77.base.sys.file.I_File;
import de.mn77.base.sys.file.SysDir;
import de.mn77.ext.pdf.create.PDF_Document;
import de.mn77.ext.pdf.create.constant.PDF_FONT;
import de.mn77.ext.pdf.create.constant.PDF_PAGEFORMAT;
import de.mn77.ext.pdf.create.page.PDF_Page;
import de.mn77.lib.graphic.MImage;


/**
 * @author Michael Nitsche
 */
public class Test_PDF_Create {

	public static void main( final String[] args ) {

		try {
			final I_File file = SysDir.temp().fileMay( "testpdf", "pdf" );

			final PDF_Document document = new PDF_Document( file );

			// PAGE 1

			final PDF_Page page1 = document.newPage();
			page1.newText( 50, 50, "Seite 1" );
			page1.newText( 50, 75, "Dieser Text ist frei positioniert!" );

			page1.newText( 0, 150, "Dieser Text ist linksbündig" );
			page1.newText( page1.getWidth() / 2, 150, "Dieser Text ist mittig" ).setAlignment( POSITION_H.CENTER );
			page1.newText( page1.getWidth(), 150, "Dieser Text ist rechtsbündig" ).setAlignment( POSITION_H.RIGHT );

			// PAGE 2

			final PDF_Page page2 = document.newPage().setFormat( PDF_PAGEFORMAT.A5 ).setOrientation( true );
			page2.newLine( 10, 100, 300, 100 );
			page2.setFontColor( 0, 0, 255 );
			page2.newText( 30, 100, "ABCDEFGH" ).setFont( PDF_FONT.COURIER ).setFontsize( 16 );
			page2.newText( 150, 100, "abcdefgh" ).setFont( PDF_FONT.HELVETICA ).setFontsize( 32 ).setColor( 255, 0, 0 );

			// PAGE 3

			document.newPage().setFormat( PDF_PAGEFORMAT.A4 ).setOrientation( false ).setBackground( 255, 255, 0 );
			document.newParagraph( "Seite 3" );

			// PAGE 4

			document.newPage().setOrientation( true ).setBackground( 255, 0, 255 ).setFormat( PDF_PAGEFORMAT.A4 );
			document.newParagraph( "Seite 4" );

			// PAGE 5

			final PDF_Page page5 = document.newPage().setBackground( 255, 255, 255 ).setOrientation( false ).setFormat( PDF_PAGEFORMAT.A4 );
			document.newText( "Seite5" ).setBackground( 0, 0, 255 );
			page5.newLine( 10, 100, 300, 300 ).setColor( 255, 100, 0 );
			page5.newLine( 100, 80, 200, 80 ).setColor( 255, 100, 255 );
			page5.newLine( 50, 350, 500, 350 ).setColor( 0, 255, 0 );
//
			page5.setThickness( 3 );

			page5.setLineColor( 100, 100, 255 ).newRectangle( 50, 200, 100, 100 );
			page5.setFillColor( 0, 255, 0 ).newRectangle( 60, 210, 100, 100 ).setStyle( true, false );
			page5.setFillColor( 255, 100, 0 ).newCircle( 200, 200, 50 ).setStyle( true, false );
			page5.setFillColor( 255, 100, 0 ).newCircle( 300, 300, 50 ).setStyle( false, true );

			// PAGE 6

			document.newPage();
			document.newText( "Dies ist ein Textelement!" ).setBackground( 0, 0, 255 );
			document.newText( "Dies ist ein Textelement!" ).setBackground( 255, 0, 0 );
			document.newParagraph( "Dies ist ein Absatz" );
			document.newParagraph( "Dies ist ein Absatz" );

			// PAGE 7

			final PDF_Page page7 = document.newPage();
			document.newText( "Seite 7" );
			page7.newImage( new MImage( 100, 100, 255, 100, 100 ), 100, 100 );
			page7.newImage( new MImage( 100, 100, 100, 255, 100 ), 200, 200 );
			page7.newImage( new MImage( 100, 100, 100, 100, 255 ), 300, 100 );
//			seite7.n_Bild(E_Bild("/home/mike/temp/NATURE-Phlox_1600x1200.jpg").aufloesung(400,400),100,400);

			// END

			document.close();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
