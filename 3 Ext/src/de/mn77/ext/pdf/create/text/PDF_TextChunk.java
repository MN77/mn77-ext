/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create.text;

import java.awt.Color;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class PDF_TextChunk implements I_PDF_TextItem<PDF_TextParagraph> {

	private String text = null;
	private int[]  backgroundRGB;


	public PDF_TextChunk( final String text ) {
		Err.ifEmpty( text );
		this.text = text;
		this.backgroundRGB = new int[]{ 255, 255, 255 };
	}

	public void draw( final Document dokument ) {
		final Chunk result = new Chunk( this.text );
//		if(this.hintergrund[0]!=255 || this.hintergrund[1]!=255 || this.hintergrund[2]!=255)
		result.setBackground( new Color( this.backgroundRGB[0], this.backgroundRGB[1], this.backgroundRGB[2] ) );

		try {
			dokument.add( result );
		}
		catch( final DocumentException e ) {
			Err.newRuntime( "PDF-Error", e );
		}

//		float subscript = -8.0f;
//		pdf().setTextRise(subscript);
//		pdf().setUnderline(new Color(0xFF, 0x00, 0x00), 3.0f, 0.0f, -5.0f + subscript, 0.0f, PdfContentByte.LINE_CAP_ROUND);
	}

	public PDF_TextChunk setBackground( final int r, final int g, final int b ) {
		Err.ifOutOfBounds( 0, 255, r );
		Err.ifOutOfBounds( 0, 255, g );
		Err.ifOutOfBounds( 0, 255, b );
		this.backgroundRGB = new int[]{ r, g, b };
		return this;
	}

}
