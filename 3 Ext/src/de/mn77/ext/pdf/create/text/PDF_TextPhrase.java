/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create.text;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class PDF_TextPhrase implements I_PDF_TextItem<PDF_TextPhrase> {

	private String text = null;


	public PDF_TextPhrase( final String text ) {
		Err.ifEmpty( text );
		this.text = text;
	}

	public void draw( final Document document ) {
		final Phrase erg = new Phrase( this.text );

//		if(this.hintergrund[0]!=255 || this.hintergrund[1]!=255 || this.hintergrund[2]!=255)
//			erg.setBackground(new Color(this.hintergrund[0],this.hintergrund[1],this.hintergrund[2]));
		try {
			document.add( erg );
		}
		catch( final DocumentException e ) {
			Err.newRuntime( "PDF-Error", e );
		}
	}

}
