/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_File;
import de.mn77.ext.pdf.create.page.PDF_Page;
import de.mn77.ext.pdf.create.text.I_PDF_TextItem;
import de.mn77.ext.pdf.create.text.PDF_TextChunk;
import de.mn77.ext.pdf.create.text.PDF_TextParagraph;


/**
 * @author Michael Nitsche
 */
public class PDF_Document {

	private OutputStream       stream;
	private final List<Object> items;


	public PDF_Document( final File pdfFile ) throws Err_FileSys {
		Err.ifNull( pdfFile );

		try {
			this.stream = new FileOutputStream( pdfFile );
		}
		catch( final FileNotFoundException e ) {
			throw Err.wrap( e, "File not found" );
		}

		this.items = new SimpleList<>();
	}

	public PDF_Document( final I_File pdfFile ) throws Err_FileSys {
		Err.ifNull( pdfFile );
		this.stream = pdfFile.write();
		this.items = new SimpleList<>();
	}

	public PDF_Document( final String pdfFile ) throws Err_FileSys {
		Err.ifNull( pdfFile );

		try {
			this.stream = new FileOutputStream( pdfFile );
		}
		catch( final FileNotFoundException e ) {
			Err.wrap( e, pdfFile );
		}

		this.items = new SimpleList<>();
	}

	public void close() {
		Document document = null;
		PdfWriter writer = null;

		for( final Object item : this.items )
			if( item instanceof final PDF_Page page ) {
				Rectangle rect = page.getFormat().getRectangle();
				if( page.isLandscape() )
					rect = rect.rotate();

				try {

					if( document == null ) {
						document = new Document( page.getFormat().getRectangle() );
						writer = PdfWriter.getInstance( document, this.stream );
						writer.setPdfVersion( PdfWriter.VERSION_1_2 ); // Default = 1.2 (1.6 is necessary Acrobat >= 6)
						document.open();
					}
					else {
						document.setPageSize( rect );
						document.newPage();
						writer.setPageEmpty( false );
					}

					document.add( new Chunk( "" ) ); // Necessary for background color
					page.draw( writer.getDirectContent() );
				}
				catch( final DocumentException e ) {
					throw Err.direct( "PDF-Error", e );
				}
			}
			else
				((I_PDF_TextItem<?>)item).draw( document );

		document.close();
	}

	public PDF_Page newPage() { //TODO Aktuelle Formatvorgaben übergeben
		final PDF_Page result = new PDF_Page();
		this.items.add( result );
		return result;
	}

	public PDF_TextParagraph newParagraph( final String text ) { //TODO Aktuelle Formatvorgaben übergeben
		final PDF_TextParagraph result = new PDF_TextParagraph( text );
		this.items.add( result );
		return result;
	}

//	public S_PDF_FT_Gruppe newTextGroup(String text)   { //TODO Aktuelle Formatvorgaben übergeben
//		S_PDF_FT_Gruppe result = new PDF_FT_Gruppe(text);
//		this.items.add(result);
//		return result;
//	}

	public PDF_TextChunk newText( final String text ) { //TODO Aktuelle Formatvorgaben übergeben
		final PDF_TextChunk result = new PDF_TextChunk( text );
		this.items.add( result );
		return result;
	}

}
