/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create.constant;

import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;


/**
 * @author Michael Nitsche
 */
public enum PDF_PAGEFORMAT {

	A0( PageSize.A0 ),
	A1( PageSize.A1 ),
	A2( PageSize.A2 ),
	A3( PageSize.A3 ),
	A4( PageSize.A4 ),
	A5( PageSize.A5 ),
	A6( PageSize.A6 );


	private final Rectangle size;


	PDF_PAGEFORMAT( final Rectangle size ) {
		this.size = size;
	}

	public Rectangle getRectangle() {
		return this.size;
	}

}
