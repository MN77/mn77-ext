/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.create.constant;

import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public enum PDF_FONT {

	HELVETICA( FontFactory.HELVETICA, FontFactory.HELVETICA_BOLD, FontFactory.HELVETICA_OBLIQUE, FontFactory.HELVETICA_BOLDOBLIQUE ),
	COURIER( FontFactory.COURIER, FontFactory.COURIER_BOLD, FontFactory.COURIER_OBLIQUE, FontFactory.COURIER_BOLDOBLIQUE ),
	TIMES( FontFactory.TIMES, FontFactory.TIMES_BOLD, FontFactory.TIMES_ITALIC, FontFactory.TIMES_BOLDITALIC );


	private final String regular, bold, italic, boldItalic;


	PDF_FONT( final String regular, final String fett, final String kursiv, final String boldItalic ) {
		this.regular = regular;
		this.bold = fett;
		this.italic = kursiv;
		this.boldItalic = boldItalic;
	}

	public BaseFont getBaseFont( final boolean bold, final boolean italic ) {
		BaseFont result = null;

		try {
			result = BaseFont.createFont( this.getName( bold, italic ), BaseFont.CP1252, BaseFont.NOT_EMBEDDED );
		}
		catch( final DocumentException e ) {
			throw Err.newRuntime( "PDF-Error", e );
		}
		catch( final IOException e ) {
			throw Err.newRuntime( "Font not available!", e, this.getName( bold, italic ) );
		} // Kommt vmtl. seeehr selten vor!

		Err.ifNull( result );
		return result;
	}

	public String getName( final boolean bold, final boolean italic ) {
		if( !bold && !italic )
			return this.regular;
		if( bold && !italic )
			return this.bold;
		if( !bold && italic )
			return this.italic;
		return this.boldItalic;
	}

}
