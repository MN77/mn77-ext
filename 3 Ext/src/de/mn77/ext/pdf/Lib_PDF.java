/*******************************************************************************
 * Copyright (C) 2009-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf;

/**
 * @author Michael Nitsche
 * @created 22.03.2009
 */
public class Lib_PDF {

	public static float cm2p( final double cm ) {
		return (float)(cm * 28.25); //in der Breite wärens eigentlich 28.18 oder so!   28.25 ist in der Höhe ok
//		return (float)(cm / 2.54 * 72); //Offizielle Umrechnung! Entspricht also 28.346! Ist nur leider zu groß
	}

}
