/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.fill;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Function;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_File;


/*
 * http://dobbse.net/thinair/2004/02/pdf-pot-luck.html
 */
public class PDF_Filler {

	private final Object source;
	private boolean      debug = false;


	public PDF_Filler( final Function<?, InputStream> source_pdf ) {
		this.source = source_pdf;
	}

	public PDF_Filler( final InputStream source_pdf ) {
		this.source = source_pdf;
	}

	public PDF_Filler( final String source_pdf ) {
		this.source = source_pdf;
	}


	// SET

	/**
	 * Nur Schlüssel in Kleinbuchstaben verwenden!!! Akzeptiert wird aber alles
	 */
	public void fill( final FileOutputStream target, final Map<String, String> data ) throws DocumentException, Err_FileSys {
		this.iFillSinglePage( target, data, false );
	}

	// GET

	/**
	 * Nur Schlüssel in Kleinbuchstaben verwenden!!! Akzeptiert wird aber alles
	 * Für mehrere Seiten
	 *
	 * TODO Man könnte natürlich auch das Template irgendwie im Speicher übergeben...
	 *
	 * Temp-File wird für einzelne Seiten verwendet, die dann zum Hauptdokument hinzugefügt werden.
	 */
	public void fill( final FileOutputStream target, final String tmpFile, final I_Iterable<Map<String, String>> pages, final PDF_FillInfo info ) throws DocumentException, Err_FileSys {
		if( !(this.source instanceof Function || this.source instanceof String) )
			Err.forbidden( "Für mehrere Seiten werden mehrere Streams auf das Template benötigt!" );

		try {
			final PdfReader reader = this.iReader();
			final Document document = new Document( reader.getPageSizeWithRotation( 1 ) );
			final PdfCopy pdfcopy = new PdfCopy( document, target );

			if( info.PDF_VERSION.get() != null )
				pdfcopy.setPdfVersion( info.PDF_VERSION.get() );
			if( info.PDF_FORMAT.get() != null )
				pdfcopy.setPDFXConformance( info.PDF_FORMAT.get() ); //PdfCopy.PDFA1B
//			pdfcopy.setTagged();

			if( info.COMPRESS.get() )
				pdfcopy.setFullCompression();

			document.open();

			if( info.AUTHOR.get() != null )
				document.addAuthor( info.AUTHOR.get() );
			if( info.BETREFF.get() != null )
				document.addSubject( info.BETREFF.get() );
			document.addCreationDate();
			if( info.ERSTELLER.get() != null )
				document.addCreator( info.ERSTELLER.get() );
			if( info.TITEL.get() != null )
				document.addTitle( info.TITEL.get() );
			if( info.PDF_FORMAT.get() != null )
				pdfcopy.createXmpMetadata();
//          document.addLanguage("de-de");

//          document.open();
			for( final Map<String, String> page : pages ) {
				final PdfStamper stamp = this.iFillSinglePage( new FileOutputStream( tmpFile ), page, true );
				final PdfReader newPageReader = new PdfReader( tmpFile );
				pdfcopy.addPage( stamp.getImportedPage( newPageReader, 1 ) );
				new File( tmpFile ).delete();
			}

//            pdfcopy.setSigFlags(arg0);

			document.close();
			pdfcopy.close();
			reader.close();

//			VERSION 1 - Ok, aber die Feldernamen werden verdoppelt!
//			this.reader.close();
//			String quell="/home/mike/Projekte/Sammet Camo+/Formular-Test/Form2.pdf";
//			PdfReader reader1 = new PdfReader(quell);
//			PdfReader reader2 = new PdfReader(quell);
//			PdfCopyFields copy = new PdfCopyFields(new FileOutputStream("/tmp/concatenatedPDF.pdf"));
//			copy.addDocument(reader1);
//			copy.addDocument(reader2);
//			copy.close();
//			this.reader = new PdfReader("/tmp/concatenatedPDF.pdf");
		}
		catch( final IOException e ) {
			throw Err.wrap( e, "Dateifehler" );
		}
	}


	// FILL - Single Page	(Achtung! - Dokument ist weiter veränderbar)

	public void fill( final I_File target, final Map<String, String> data ) throws DocumentException, Err_FileSys {
		this.fill( target.write(), data );
	}

	public void fill( final I_File target, final String tmp_file, final I_Iterable<Map<String, String>> data, final PDF_FillInfo info ) throws DocumentException, Err_FileSys {
		this.fill( target.write(), tmp_file, data, info );
	}

	public void fill( final String target, final Map<String, String> data ) throws DocumentException, Err_FileSys {

		try {
			this.fill( new FileOutputStream( target ), data );
		}
		catch( final FileNotFoundException e ) {
			Err.wrap( e, "Datei existiert nicht!" );
		}
	}

	public void fill( final String target, final String tmp_file, final I_Iterable<Map<String, String>> data, final PDF_FillInfo info ) throws DocumentException, Err_FileSys {

		try {
			this.fill( new FileOutputStream( target ), tmp_file, data, info );
		}
		catch( final FileNotFoundException e ) {
			Err.wrap( e, "Datei existiert nicht!" );
		}
	}


	// FILL - Multi Page

	public I_Iterable<String> getFieldNames() throws IOException {
		final I_List<String> result = new SimpleList<>();
//		MOut.print(this.source);
		Err.ifNull( this.iReader() );
		final PdfReader reader = this.iReader();
		if( reader.getAcroForm() == null ) // Form has no Fields!!!
//			throw ErrorExec();
			return result;

		for( final Iterator<PRAcroForm.FieldInformation> i = reader.getAcroForm().getFields().iterator(); i.hasNext(); ) {
			final PRAcroForm.FieldInformation field = i.next();
			result.add( field.getName() );
		}

		return result;
	}

	public void setDebug( final boolean b ) {
		this.debug = b;
	}

	private PdfStamper iFillSinglePage( final FileOutputStream dest_pdf, final Map<String, String> data, final boolean flattening ) throws DocumentException, Err_FileSys {

		try {
			final PdfReader reader = this.iReader();
			final PdfStamper stamp = new PdfStamper( reader, dest_pdf );

			stamp.setFullCompression();

			if( flattening )
				stamp.setFormFlattening( true ); //Macht die Datei fast doppelt so groß. Aber bei einer Seite sonst kein Unterschied. Bei mehreren Pflicht
			final AcroFields form = stamp.getAcroFields();

			for( final Iterator<PRAcroForm.FieldInformation> i = reader.getAcroForm().getFields().iterator(); i.hasNext(); ) {
				final PRAcroForm.FieldInformation field = i.next();
				final String feldname = field.getName();
//					MOut.dev(feldname, daten.get(feldname));
				if( data.keySet().contains( feldname ) )
					form.setField( feldname, data.get( feldname ) );
				else if( this.debug )
					MOut.dev( "Kein Text für Feld gefunden: " + feldname );
			}

			//		stamp.setFullCompression();
			stamp.close();
			return stamp;
		}
		catch( final IOException e ) {
			throw Err.wrap( e, "Dateifehler" );
		}
	}

	// PRIVATE

	private PdfReader iReader() throws IOException {
		if( this.source instanceof String )
			return new PdfReader( (String)this.source );
		if( this.source instanceof InputStream )
			return new PdfReader( (InputStream)this.source );
		if( this.source instanceof Function )
			return new PdfReader( ((Function<?, InputStream>)this.source).apply( null ) );
		throw Err.impossible();
	}

}
