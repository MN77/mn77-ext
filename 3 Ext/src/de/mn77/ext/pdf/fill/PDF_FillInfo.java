/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.fill;

import com.lowagie.text.pdf.PdfName;

import de.mn77.base.data.constant.ACCESS;
import de.mn77.base.data.container.Box;


/**
 * @author Michael Nitsche
 * @created 26.02.2019
 */
public class PDF_FillInfo {

	public final Box<String> TITEL     = new Box<>( ACCESS.FULL );
	public final Box<String> BETREFF   = new Box<>( ACCESS.FULL );
	public final Box<String> AUTHOR    = new Box<>( ACCESS.FULL );
	public final Box<String> ERSTELLER = new Box<>( ACCESS.FULL );

	public final Box<PdfName> PDF_VERSION = new Box<>( ACCESS.FULL ); // PdfCopy.PDF_VERSION_1_7 , PdfCopy.PDF_VERSION_1_4
	public final Box<Integer> PDF_FORMAT  = new Box<>( ACCESS.FULL ); // PdfCopy.PDFA1B
	public final Box<Boolean> COMPRESS    = new Box<>( ACCESS.FULL, false );

}
