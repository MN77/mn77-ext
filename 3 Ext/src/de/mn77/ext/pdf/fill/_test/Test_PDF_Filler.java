/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MN77-External-Library <https://www.mn77.de>
 *
 * MN77-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MN77-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MN77-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.ext.pdf.fill._test;

import java.util.HashMap;
import java.util.Map;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.ext.pdf.fill.PDF_FillInfo;
import de.mn77.ext.pdf.fill.PDF_Filler;


public class Test_PDF_Filler {

	public static void main( final String[] args ) {

		try {
			Test_PDF_Filler.start();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	public static void start() throws Throwable {
//		PDF_Fuellen pdf=new PDF_Fuellen("/home/mike/Projekte/ScheibeAircraft Camo+/Formular-Test/Gewicht3.pdf");
		final PDF_Filler pdf = new PDF_Filler( "/home/mike/Projekte/Sammet Camo+/Formulare/Vorlagen/LTA-Übersicht/Lta-Über3.pdf" );

		//MOut 1
		final String s = pdf.getFieldNames().toDescribe();
		MOut.print( s );

		//MOut 2
		final Map<String, String> data = new HashMap<>();
		data.put( "kennzeichen", "D-1231" );
		data.put( "kennblatt", "12345" );
		data.put( "muster", "ASK 21" );
		data.put( "benennung", "Bla" );
		data.put( "einbau5", "2005-02-11" );
		pdf.fill( "/tmp/ziel1.pdf", data );

		//MOut 3
		final I_List<Map<String, String>> pages = new SimpleList<>();

		for( int num = 1; num <= 5; num++ ) {
			final Map<String, String> data2 = new HashMap<>();
			pages.add( data2 );

			data2.put( "kennzeichen", "D-123" + num );
			data2.put( "muster", "ASK 2" + num );
			data2.put( "benennung", "Bla " + num );
			data2.put( "einbau5", "2005-02-1" + num );
		}

		final PDF_FillInfo info = new PDF_FillInfo();
		info.TITEL.set( "Test-PDF" );
		info.BETREFF.set( "Test-Betreff" );
		info.AUTHOR.set( "Test-Author" );
		info.ERSTELLER.set( "Test-Ersteller" );
		pdf.fill( "/tmp/ziel2.pdf", "/tmp/temp.pdf", pages, info );
	}

}
